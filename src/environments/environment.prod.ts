export const environment = {
  production: true,
  modules: [],
  apiUrl: 'https://portal.organicfruitapps.com/programming-guides/v2/us_en-us',
};
