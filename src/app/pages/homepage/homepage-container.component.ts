import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-homepage-container',
  templateUrl: './homepage-container.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomepageContainerComponent {}
