import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageContainerComponent } from './homepage-container.component';

const routes: Routes = [
  {
    path: '',
    component: HomepageContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class HomepageRoutingModule {}
