import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedPlaylistsContainerComponent } from '../featured-playlists-container.component';
import { provideMockStore } from '@ngrx/store/testing';
import { Component, Input } from '@angular/core';
import { FeaturedPlaylist } from '../featured-playlists.interfaces';
import { MOCK_FEATURED_PLAYLIST_1 } from '../mock-data/featured-playlists-mocks';

describe('FeaturedPlaylistsContainerComponent', () => {
  let component: FeaturedPlaylistsContainerComponent;
  let fixture: ComponentFixture<FeaturedPlaylistsContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        TestFeaturedPlaylistsComponent,
        FeaturedPlaylistsContainerComponent
      ],
      providers: [
        provideMockStore({ initialState: {
          featuredPlaylists: {
            isLoadingFeaturedPlaylists: false,
            featuredPlaylists: [MOCK_FEATURED_PLAYLIST_1]
          }
          } }),
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedPlaylistsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(fixture).toMatchSnapshot();
  });
});


@Component({
  selector: 'app-featured-playlists',
  template: `
    featuredPlaylists: {{ featuredPlaylists | json }}  <br />
    {{ isLoadingFeaturedPlaylists }}
  `
})
export class TestFeaturedPlaylistsComponent {
  @Input() featuredPlaylists!: FeaturedPlaylist[] | null;
  @Input() isLoadingFeaturedPlaylists!: boolean | null;
}
