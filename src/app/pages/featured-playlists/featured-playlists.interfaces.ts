export interface FeaturedPlaylistsEntity {
  name: string;
  content: FeaturedPlaylist[];
}

export enum FeaturedPlaylistKindType {
  playlist = 'playlist',
}

export interface FeaturedPlaylist {
  id: string;
  kind: FeaturedPlaylistKindType;
  name: string;
  url: string;
  curator_name: string;
  artwork: string;
}

export interface GetFeaturedPlaylistsApiResponse {
  featuredPlaylists: FeaturedPlaylistsEntity;
}
