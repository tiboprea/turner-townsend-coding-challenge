import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeaturedPlaylistsContainerComponent } from './featured-playlists-container.component';

const routes: Routes = [
  {
    path: '',
    component: FeaturedPlaylistsContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class FeaturedPlaylistsRoutingModule {}
