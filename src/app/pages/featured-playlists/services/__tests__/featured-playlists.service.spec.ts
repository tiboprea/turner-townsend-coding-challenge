import { TestBed } from '@angular/core/testing';

import { FeaturedPlaylistsService } from '../featured-playlists.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MOCK_GET_FEATURED_PLAYLISTS_API_RESPONSE } from '../../mock-data/featured-playlists-mocks';
import { environment } from '../../../../../environments/environment';

describe('FeaturedPlaylistsService', () => {
  let service: FeaturedPlaylistsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FeaturedPlaylistsService],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(FeaturedPlaylistsService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getFeaturedPlaylists', () => {
    it('should return the featured playlists', () => {
      service.getFeaturedPlaylists().subscribe(featuredPlaylistsApiResponse => {
        const featuredPlaylists = featuredPlaylistsApiResponse.featuredPlaylists;
        expect(featuredPlaylistsApiResponse).toEqual(MOCK_GET_FEATURED_PLAYLISTS_API_RESPONSE);
        expect(featuredPlaylists.content.length).toBe(2);
      });

      const req = httpMock.expectOne(`${environment.apiUrl}/featured-playlists.json`);
      expect(req.request.method).toBe("GET");
      req.flush(MOCK_GET_FEATURED_PLAYLISTS_API_RESPONSE);
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
