import { Injectable } from '@angular/core';
import { HttpService } from '../../../core';
import { Observable } from 'rxjs';
import { GetFeaturedPlaylistsApiResponse } from '../featured-playlists.interfaces';

@Injectable()
export class FeaturedPlaylistsService {
  constructor(private readonly httpService: HttpService) {
  }

  getFeaturedPlaylists(): Observable<GetFeaturedPlaylistsApiResponse> {
    return this.httpService.get('featured-playlists.json',);
  }
}
