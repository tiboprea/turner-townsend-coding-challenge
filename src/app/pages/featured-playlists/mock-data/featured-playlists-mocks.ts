import {
  FeaturedPlaylist, FeaturedPlaylistKindType,
  FeaturedPlaylistsEntity,
  GetFeaturedPlaylistsApiResponse
} from '../featured-playlists.interfaces';

export const MOCK_FEATURED_PLAYLIST_1: FeaturedPlaylist = {
  "id": "pl.2b0e6e332fdf4b7a91164da3162127b5",
  "kind": FeaturedPlaylistKindType.playlist,
  "name": "New Music Daily",
  "url": "https://music.apple.com/us/playlist/new-music-daily/pl.2b0e6e332fdf4b7a91164da3162127b5?app=music&at=1000l4QJ&ct=330&itscg=10000&itsct=330",
  "curator_name": "Apple Music",
  "artwork": "https://is2-ssl.mzstatic.com/image/thumb/Features116/v4/9b/46/14/9b461442-e977-1fb4-3aac-a4018a7effbb/U0MtTVMtV1ctTk1ELU9kZXN6YS1BREFNX0lEPTEwNTMxNjU4NTcucG5n.png/600x600SC.DN01.jpg?l=en-US"
};

export const MOCK_FEATURED_PLAYLIST_2: FeaturedPlaylist =  {
  "id": "pl.f4d106fed2bd41149aaacabb233eb5eb",
  "kind": FeaturedPlaylistKindType.playlist,
  "name": "Today’s Hits",
  "url": "https://music.apple.com/us/playlist/todays-hits/pl.f4d106fed2bd41149aaacabb233eb5eb?app=music&at=1000l4QJ&ct=330&itscg=10000&itsct=330",
  "curator_name": "Apple Music Hits",
  "artwork": "https://is1-ssl.mzstatic.com/image/thumb/Features126/v4/32/50/53/325053b6-ad99-12da-7fe7-d98bebd9d297/U0MtTVMtV1ctVG9kYXlzSGl0cy1OaWNraW1pbmFqbGlsYmFieS1BREFNX0lEPTEwMTA0MTc4MTYucG5n.png/600x600SC.DN01.jpg?l=en-US"
}

export const MOCK_FEATURED_PLAYLISTS_ENTITY: FeaturedPlaylistsEntity = {
  name: 'Featured Playlists',
  content: [MOCK_FEATURED_PLAYLIST_1, MOCK_FEATURED_PLAYLIST_2],
}

export const MOCK_GET_FEATURED_PLAYLISTS_API_RESPONSE : GetFeaturedPlaylistsApiResponse = {
  featuredPlaylists: MOCK_FEATURED_PLAYLISTS_ENTITY
}
