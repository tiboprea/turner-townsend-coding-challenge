import { createAction, props } from '@ngrx/store';
import { FeaturedPlaylist } from '../featured-playlists.interfaces';

export const getFeaturedPlaylists = createAction(
  '[Featured Playlists] GET_FEATURED_PLAYLISTS',
);
export const getFeaturedPlaylistsSuccess = createAction(
  '[Featured Playlists] GET_FEATURED_PLAYLISTS_SUCCESS',
  props<{ featuredPlaylists: FeaturedPlaylist[] }>(),
);
export const getFeaturedPlaylistsError = createAction('[Featured Playlists] GET_FEATURED_PLAYLISTS_ERROR');


export const clearFeaturedPlaylistState = createAction('[Featured Playlists] CLEAR_STATE');
