import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, switchMap } from 'rxjs/operators';
import { FeaturedPlaylistsService } from '../services/featured-playlists.service';
import {
  getFeaturedPlaylists,
  getFeaturedPlaylistsError,
  getFeaturedPlaylistsSuccess
} from './featured-playlists.actions';
import { AppState } from '../../../core';

@Injectable()
export class FeaturedPlaylistsEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly store: Store<AppState>,
    private readonly featuredPlaylistsService: FeaturedPlaylistsService,
  ) {}

  getAllAuctionItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getFeaturedPlaylists),
      switchMap(() => {
        return this.featuredPlaylistsService
          .getFeaturedPlaylists()
          .pipe(
            switchMap(({featuredPlaylists}) => {
              return [getFeaturedPlaylistsSuccess({ featuredPlaylists: featuredPlaylists.content })];
            }),
            catchError((error) => {
              return [getFeaturedPlaylistsError()];
            }),
          );
      }),
    ),
  );
}
