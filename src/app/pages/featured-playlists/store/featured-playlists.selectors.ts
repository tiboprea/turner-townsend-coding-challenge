import { featuredPlaylistsFeature } from './featured-playlists.reducer';

const {
  selectIsLoadingFeaturedPlaylists,
  selectFeaturedPlaylists,
} = featuredPlaylistsFeature;

export const featuredPlaylistsQuery = {
  selectIsLoadingFeaturedPlaylists,
  selectFeaturedPlaylists,
};
