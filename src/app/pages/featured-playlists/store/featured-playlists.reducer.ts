import { createFeature, createReducer, on } from '@ngrx/store';
import { FeaturedPlaylist } from '../featured-playlists.interfaces';
import {
  clearFeaturedPlaylistState,
  getFeaturedPlaylists,
  getFeaturedPlaylistsError,
  getFeaturedPlaylistsSuccess
} from './featured-playlists.actions';

export interface FeaturedPlaylists {
  isLoadingFeaturedPlaylists: boolean;
  featuredPlaylists: FeaturedPlaylist[];
}

export const featuredPlaylistsInitialState: FeaturedPlaylists = {
  isLoadingFeaturedPlaylists: false,
  featuredPlaylists: [],
};

export const featuredPlaylistsFeature = createFeature({
  name: 'featuredPlaylists',
  reducer: createReducer(
    featuredPlaylistsInitialState,
    on(getFeaturedPlaylists, (state) => ({
      ...state,
      isLoadingFeaturedPlaylists: true,
    })),
    on(getFeaturedPlaylistsSuccess, (state, { featuredPlaylists }) => ({
      ...state,
      isLoadingFeaturedPlaylists: false,
      featuredPlaylists
    })),
    on(getFeaturedPlaylistsError, (state) => ({
      ...state,
      isLoadingFeaturedPlaylists: false,
    })),
    on(clearFeaturedPlaylistState, () => ({
      ...featuredPlaylistsInitialState,
    })),
  ),
});
