import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { AppState } from '../../core';
import { clearFeaturedPlaylistState, getFeaturedPlaylists } from './store/featured-playlists.actions';
import { Store } from '@ngrx/store';
import { FeaturedPlaylist } from './featured-playlists.interfaces';
import { Observable } from 'rxjs';
import { featuredPlaylistsQuery } from './store/featured-playlists.selectors';

@Component({
  selector: 'app-featured-playlists-container',
  templateUrl: './featured-playlists-container.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeaturedPlaylistsContainerComponent implements OnInit, OnDestroy {
  featuredPlaylists$!: Observable<FeaturedPlaylist[]>;
  isLoadingFeaturedPlaylists$!: Observable<boolean>;

  constructor(private readonly store: Store<AppState>) {}

  ngOnInit() {
    this.featuredPlaylists$ = this.store.select(featuredPlaylistsQuery.selectFeaturedPlaylists);
    this.isLoadingFeaturedPlaylists$ = this.store.select(featuredPlaylistsQuery.selectIsLoadingFeaturedPlaylists);

    this.store.dispatch(getFeaturedPlaylists());
  }

  ngOnDestroy() {
    this.store.dispatch(clearFeaturedPlaylistState());
  }
}
