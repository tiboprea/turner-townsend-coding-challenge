import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturedPlaylistsContainerComponent } from './featured-playlists-container.component';
import { FeaturedPlaylistsRoutingModule } from './featured-playlists-routing.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { featuredPlaylistsFeature } from './store/featured-playlists.reducer';
import { FeaturedPlaylistsEffects } from './store/featured-playlists.effects';
import { FeaturedPlaylistsService } from './services/featured-playlists.service';
import { FeaturedPlaylistsComponent } from './components';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [
    FeaturedPlaylistsContainerComponent,
    FeaturedPlaylistsComponent
  ],
  imports: [
    CommonModule,
    FeaturedPlaylistsRoutingModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatButtonModule,
    StoreModule.forFeature(featuredPlaylistsFeature),
    EffectsModule.forFeature([FeaturedPlaylistsEffects]),
  ],
  providers: [FeaturedPlaylistsService]
})
export class FeaturedPlaylistsModule { }
