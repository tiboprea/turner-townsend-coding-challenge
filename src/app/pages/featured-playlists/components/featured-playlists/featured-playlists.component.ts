import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FeaturedPlaylist } from '../../featured-playlists.interfaces';

@Component({
  selector: 'app-featured-playlists',
  templateUrl: './featured-playlists.component.html',
  styleUrls: ['./featured-playlists.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeaturedPlaylistsComponent  {
  @Input() featuredPlaylists!: FeaturedPlaylist[] | null;
  @Input() isLoadingFeaturedPlaylists!: boolean | null;

  trackByPlaylistId(index: number, item: FeaturedPlaylist) {
    return item.id;
  }
}
