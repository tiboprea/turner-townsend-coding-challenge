import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/homepage/homepage.module').then((m) => m.HomepageModule),
  },
  {
    path: 'featured-playlists',
    loadChildren: () => import('./pages/featured-playlists/featured-playlists.module').then((m) => m.FeaturedPlaylistsModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
