import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Routes } from '../../../../core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  HOMEPAGE_ROUTE = Routes.HOMEPAGE;
  FEATURED_PLAYLISTS_ROUTE = Routes.FEATURED_PLAYLISTS;
}
