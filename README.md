# Turner Townsend Coding Challenge

First of all, thank you for sending me this coding challenge.

Notes:
- I included ngrx router store as that's part of my usual project set-up which is usually useful as the project grows
- Infinite scrolling & pagination would have been nice on the featured playlists page
- Prettier & Linter would have also been nice but I wanted to timebox the task to 2 hours as you suggested
- I would have personally used NX to set-up the project so that we can easily install Cypress & Jest, but that would have been an overkill for this task
- I usually use marble testing for testing the effects; not sure what you guys are using

Sorry for not making it prettier, I'm not a huge fan of writing CSS :)

Hope to talk to you guy soon.
